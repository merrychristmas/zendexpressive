<?php

use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Router\ZendRouter;

return [
    'dependencies' => [
        'invokables' => [
            RouterInterface::class => ZendRouter::class,
        ],
    ],
    'db' => [
        'driver'    => 'Pdo_Pgsql',
        'database'  => 'aspen',
        'username'  => 'postgres',
        'password'  => 'pgsql',
//        'hostname'  => '192.168.2.20',
        'hostname'  => '169.254.192.182',
        'port'      => '5432'
    ],
];
