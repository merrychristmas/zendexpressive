<?php

namespace User\Model;

use Zend\Crypt\Password\Bcrypt;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Filter\StripTags;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Db\NoRecordExists;
use Zend\Validator\StringLength;

class User
{
    protected $id;
    protected $username;
    protected $password;
    protected $email;

    private $inputFilter;
    private $adapter;

    public function __construct(AdapterInterface $adapter = null)
    {
        $this->adapter = $adapter;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {

        if (empty($this->id)) {
            $password = $this->encryptPassword($password);
        }

        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getInputFilter()
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }

        $inputFilter = new InputFilter();

        $inputFilter->add([
            'name' => 'username',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 5,
                        'max' => 10
                    ]
                ], [
                    'name' => NoRecordExists::class,
                    'options' => [
                        'table' => 'user',
                        'field' => 'username',
                        'adapter' => $this->adapter
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name' => 'email',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StripTags::class]
            ],
            'validators' => [
                [
                    'name' => NoRecordExists::class,
                    'options' => [
                        'table' => 'user',
                        'field' => 'email',
                        'adapter' => $this->adapter
                    ]
                ]
            ]
        ]);

        $this->inputFilter = $inputFilter;
        return $this->inputFilter;
    }

    public function exchangeArray(array $data)
    {
        $this->setPassword($data['password'] ?? null);

        $this->id       = $data['id'] ?? null;
        $this->username = $data['username'] ?? null;
        $this->password = $this->getPassword();
        $this->email    = $data['email'] ?? null;
    }

    public function toArray()
    {
        return [
            'id'       => $this->id,
            'username' => $this->username,
            'password' => $this->password,
            'email'    => $this->email
        ];
    }

    private function encryptPassword($password)
    {
        $bcrypt = new Bcrypt();
        $hashPassword = $bcrypt->create($password);
        return $hashPassword;
    }
}
