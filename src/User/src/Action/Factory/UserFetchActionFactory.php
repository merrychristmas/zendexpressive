<?php

namespace User\Action\Factory;

use Psr\Container\ContainerInterface;
use Services\User\UserRepository;
use User\Action\UserFetchAction;

class UserFetchActionFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $userRepository = $container->get(UserRepository::class);

        return new UserFetchAction($userRepository);
    }
}
