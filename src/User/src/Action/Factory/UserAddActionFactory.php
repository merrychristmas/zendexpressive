<?php

namespace User\Action\Factory;

use Psr\Container\ContainerInterface;
use Services\User\UserRepository;
use User\Action\UserAddAction;

class UserAddActionFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $userRepository = $container->get(UserRepository::class);

        return new UserAddAction($userRepository);
    }
}
