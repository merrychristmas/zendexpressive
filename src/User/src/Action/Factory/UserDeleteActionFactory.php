<?php

namespace User\Action\Factory;

use Psr\Container\ContainerInterface;
use Services\User\UserRepository;
use User\Action\UserDeleteAction;

class UserDeleteActionFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $userRepository = $container->get(UserRepository::class);

        return new UserDeleteAction($userRepository);
    }
}
