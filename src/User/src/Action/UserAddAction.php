<?php

namespace User\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Services\User\UserRepository;
use User\Form\UserForm;
use User\Model\User;
use Zend\Diactoros\Response\JsonResponse;

class UserAddAction implements MiddlewareInterface
{
    protected $userRepository;
    protected $userModel;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->userModel = new User($this->userRepository->getAdapter());
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * to the next middleware component to create the response.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $user = $request->getParsedBody();

        $userForm = new UserForm();
        $userForm->setData($user);
        $userForm->setInputFilter($this->userModel->getInputFilter());

        if ($userForm->isValid()) {
            $this->userModel->exchangeArray($userForm->getData());
            $data = $this->userModel->toArray();

            $result = $this->userRepository->addUser($data);

            if ($result['success'] == 1) {
                return new JsonResponse([
                    'success' => 1,
                    'data'    => $result['data'][0],
                    'message' => 'success'
                ]);
            }
        }
        return new JsonResponse([
            'success' => 0,
            'data'    => $userForm->getMessages() ?? '',
            'message' => 'failed'
        ]);
    }
}
