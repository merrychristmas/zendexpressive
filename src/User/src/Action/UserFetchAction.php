<?php

namespace User\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Services\User\UserRepository;
use User\Model\User;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\JsonResponse;

class UserFetchAction implements MiddlewareInterface
{
    protected $userRepository;
    protected $userModel;
    protected $result;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->userModel = new User();
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * to the next middleware component to create the response.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $this->userModel->setId($request->getAttribute('id') ?? null);

        if (null === $this->userModel->getId()) {
            $users = $this->userRepository->getUser([]);
            return new JsonResponse([
                'succcess' => 1,
                'users' => $users['data']
            ]);
        }

        $user = $this->userRepository->getUser(['id' => $this->userModel->getId()]);

        if (empty($user['data'])) {
            return new JsonResponse([
                'success' => 0,
                'user' => '',
                'message' => 'No user found.'
            ]);
        }

        return new JsonResponse([
            'success' => 1,
            'user' => $user['data'][0]
        ]);
    }
}
