<?php

namespace User\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Services\User\UserRepository;
use User\Model\User;
use Zend\Diactoros\Response\JsonResponse;

class UserDeleteAction implements MiddlewareInterface
{
    protected $userRepository;
    protected $userModel;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->userModel = new User($this->userRepository->getAdapter());
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * to the next middleware component to create the response.
     *
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $this->userModel->setId($request->getAttribute('id'));
        $result = $this->userRepository->deleteUser($this->userModel->getId());

        if ($result['success'] == 0) {
            return new JsonResponse([
                'success' => '0',
                'data'    => '',
                'message' => 'User ID not found.'
            ]);
        }

        return new JsonResponse([
            'success' => '1',
            'data'    => ''
        ]);
    }
}
