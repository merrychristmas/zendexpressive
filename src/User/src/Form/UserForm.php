<?php

namespace User\Form;

use Zend\Form\Element\Password;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class UserForm extends Form
{
    public function __construct($name = null, array $options = [])
    {
        parent::__construct($name, $options);

        $this->add([
            'name' => 'username',
            'type' => Text::class
        ]);

        $this->add([
            'name' => 'email',
            'type' => 'email'
        ]);

        $this->add([
            'name' => 'password',
            'type' => Password::class
        ]);
    }
}
