<?php

namespace Services;

use Services\Database\PostgresDataMapper;
use Services\User\UserRepository;
use Services\Database\PostgresDataMapper\Factory\PostgresDataMapperFactory;

class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'invokables' => [
                Database\RepositoryMap::class => Database\RepositoryMap::class
            ],
            'factories'  => [
                PostgresDataMapper::class => PostgresDataMapperFactory::class,
                UserRepository::class => User\UserRepository\Factory\UserRepositoryFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'app'    => [__DIR__ . '/../templates/app'],
                'error'  => [__DIR__ . '/../templates/error'],
                'layout' => [__DIR__ . '/../templates/layout'],
            ],

        ];
    }
}
