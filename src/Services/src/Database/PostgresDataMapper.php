<?php

namespace Services\Database;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\Sql\Delete;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class PostgresDataMapper implements DataMapperInterface
{
    /** @var AdapterInterface  */
    protected $adapter;

    protected $result = ['success' => 1];

    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getAdapter(): AdapterInterface
    {
        return $this->adapter;
    }

    public function find($params): array
    {
        $statement = $this->createSqlStatement($params);
        $this->execute($statement);

        return $this->result;
    }

    public function findAll($params)
    {
        $statement = $this->createSqlStatement($params);
        $this->execute($statement);

        return $this->result;
    }

    public function insert($params): array
    {
        $statement = $this->createSqlStatement($params);
        $this->execute($statement);

        return $this->result;
    }

    public function update($params)
    {
        // TODO: Implement update() method.
    }

    public function delete($params)
    {
        $statement = $this->createSqlStatement($params);
        $this->execute($statement);

        return $this->result;
    }

    public function execute($statement)
    {
        $driver = $this->adapter->createStatement($statement);
        $result = $driver->execute();

        if ($result instanceof ResultInterface && $result->isQueryResult()) {
            $resultSet = [];

            foreach ($result as $row) {
                $resultSet[] = $row;
            }

            $this->result['data'] = $resultSet;
        } else {
            if ($result->getGeneratedValue() || $result->getAffectedRows()) {
                $this->result['data'] = $result->getGeneratedValue() ?? $result->getAffectedRows();
            }
        }

        if (null === $this->result['data']) {
            $this->result['success'] = 0;
        }
    }

    public function createSqlStatement($params)
    {
        // $params = ['table', 'data', 'process'];

        $sql = new Sql($this->adapter, $params['table']);
        $process = $params['process'];
        $transaction = null;

        switch ($process) {
            case 'select':
                /** @var Select $transaction */
                $transaction = $sql->$process();
                $transaction->where($params['where']);

                break;
            case 'insert':
                /** @var Insert $transaction */
                $transaction = $sql->$process();
                $transaction->values($params['data']);

                return $sql->buildSqlString($transaction).' RETURNING *';
                break;
            case 'delete':
                /** @var Delete $transaction */
                $transaction = $sql->$process();
                $transaction->where($params['where']);

                break;
            case 'update':
                break;
        }

        return $sql->buildSqlString($transaction);
    }
}
