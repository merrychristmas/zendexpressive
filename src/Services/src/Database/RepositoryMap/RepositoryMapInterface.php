<?php

namespace Services\Database\RepositoryMap;

use Services\Database\PostgresDataMapper;

interface RepositoryMapInterface
{
    public function configure(PostgresDataMapper $adapter);
    public function find($params);
    public function findAll($params);
    public function insert($params);
    public function update($params);
    public function delete($params);
    public function execute($statement);
    public function createSqlStatement($params);
}
