<?php

namespace Services\Database;

use Zend\Db\Adapter\AdapterInterface;

interface DataMapperInterface
{
    public function getAdapter() : AdapterInterface;
    public function find($params) : array;
    public function findAll($params);
    public function insert($params) : array;
    public function update($params);
    public function delete($params);
    public function execute($statement);
    public function createSqlStatement($params);
}
