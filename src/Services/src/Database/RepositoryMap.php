<?php

namespace Services\Database;

use Services\Database\PostgresDataMapper;
use Services\Database\RepositoryMap\RepositoryMapInterface;

class RepositoryMap implements RepositoryMapInterface
{

    /**
     * @var DataMapperInterface
     */
    protected $dataMapper;

    public function configure(PostgresDataMapper $dataMapper)
    {
        $this->dataMapper = $dataMapper;
    }

    public function find($params)
    {
        return $this->dataMapper->find($params);
    }

    public function findAll($params)
    {
        return $this->dataMapper->findAll($params);
    }

    public function insert($params)
    {
        return $this->dataMapper->insert($params);
    }

    public function update($params)
    {
        return $this->dataMapper->update($params);
    }

    public function delete($params)
    {
        return $this->dataMapper->delete($params);
    }

    public function execute($statement)
    {
        return $this->dataMapper->execute($statement);
    }

    public function createSqlStatement($params)
    {
        return $this->dataMapper->createSqlStatement($params);
    }
}
