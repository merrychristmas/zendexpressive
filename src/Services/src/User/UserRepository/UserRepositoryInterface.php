<?php

namespace Services\User\UserRepository;

interface UserRepositoryInterface
{
    public function getTable();
    public function addUser($data);
    public function getUser(array $filters);
    public function deleteUser($id);
}
