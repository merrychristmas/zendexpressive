<?php

namespace Services\User\UserRepository\Factory;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Services\Database\PostgresDataMapper;
use Services\Database\RepositoryMap;
use Services\User\UserRepository;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserRepositoryFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dataMapper = $container->get(PostgresDataMapper::class);
        $repositoryMapper = $container->get(RepositoryMap::class);

        return new UserRepository($dataMapper, $repositoryMapper);
    }
}
