<?php

namespace Services\User;

use Services\Database\PostgresDataMapper;
use Services\Database\RepositoryMap\RepositoryMapInterface;
use Services\User\UserRepository\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    /** @var PostgresDataMapper  */
    protected $dataMapper;
    protected $repositoryMapper;
    protected $table = 'user';

    public function __construct(PostgresDataMapper $dataMapper, RepositoryMapInterface $repositoryMapper)
    {
        $this->dataMapper = $dataMapper;
        $this->repositoryMapper = $repositoryMapper;

        $this->repositoryMapper->configure($this->dataMapper);
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getAdapter()
    {
        return $this->dataMapper->getAdapter();
    }

    public function addUser($data)
    {
        unset($data['id']);

        $params = [
            'table'     => $this->table,
            'data'      => $data,
            'process'   => 'insert'
        ];

        return $this->repositoryMapper->insert($params);
    }

    public function getUser(array $filters)
    {
        $params = [
            'table'     => $this->table,
            'where'     => $filters,
            'process'   => 'select'
        ];

        return $this->repositoryMapper->find($params);
    }

    public function deleteUser($id)
    {
        $params = [
            'table'     => $this->table,
            'where'     => ['id' => $id],
            'process'   => 'delete'
        ];

        return $this->repositoryMapper->delete($params);
    }
}
