<?php

namespace UserTest\Action;

use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Services\User\UserRepository;
use User\Action\UserAddAction;
use User\Model\User;

class UserAddActionTest extends TestCase
{
    /** @var UserRepository|ObjectProphecy */
    protected $userRepository;

    /** @var UserAddAction */
    protected $middleware;

    public function setUp()
    {
        parent::setUp();

        $this->userRepository = $this->prophesize(UserRepository::class)->reveal();
        $this->middleware = new UserAddAction($this->userRepository);
    }

    public function testAttributes()
    {
        $message = strtoupper('Invalid class attribute');

        $this->assertClassHasAttribute('userRepositorysdfhsdf', UserAddAction::class, $message);
        $this->assertClassHasAttribute('userModel', UserAddAction::class, $message);

        $this->assertAttributeInstanceOf(
            UserRepository::class,
            'userRepository',
            $this->middleware, $message
        );
        $this->assertAttributeInstanceOf(User::class, 'userModel', $this->middleware, $message);
    }
}